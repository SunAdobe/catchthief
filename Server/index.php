<?php
  define('DB_HOST', 'localhost');
  define('DB_NAME', '<YOUR_DB_NAME>');
  define('DB_USER', '<YOUR_DB_USER>');
  define('DB_PASSWORD', '<YOUR_DB_PASSWORD>');



  require_once("lib/nusoap.php");

  $namespace = "http://camera.ciaddon.com/";
  $server = new soap_server();
  $server -> configureWSDL('CatchThief',$namespace);
  $server -> wsdl -> schemaTargetNamespace = $namespace;

  /*{ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! }*/
  $server -> register ('getVersion',
    array(),
    array('return' => 'xsd:string'),
	  $namespace,
    "$namespace#getVersion",
    'rpc',
    'encoded',
    'get current version of catch thief web service'
  );

  $server -> register ('setImage',
    array('username' => 'xsd:string', 'password' => 'xsd:string', 'base64Binary' => 'xsd:string'),
    array('return' => 'xsd:string'),
	  $namespace,
    "$namespace#setImage",
    'rpc',
    'encoded',
    'save webcam image per user'
  );

  $server -> register ('getImage',
    array('username' => 'xsd:string', 'password' => 'xsd:string'),
    array('return' => 'xsd:base64Binary'),
	  $namespace,
    "$namespace#getImage",
    'rpc',
    'encoded',
    'get latest webcam image per user'
  );

  /* $fullName, $userName, $emailAddress, $passWord */
  $server -> register ('regUser',
    array('fullName' => 'xsd:string',
          'userName' => 'xsd:string',
          'emailAddress' => 'xsd:string',
          'passWord' => 'xsd:string'),
    array('return' => 'xsd:string'),
	  $namespace,
    "$namespace#regUser",
    'rpc',
    'encoded',
    'register new user'
  );

  $server -> register ('sendRequest',
    array('username' => 'xsd:string', 'password' => 'xsd:string', 'message' => 'xsd:string'),
    array('return' => 'xsd:boolean'),
	  $namespace,
    "$namespace#sendRequest",
    'rpc',
    'encoded',
    'send new request to owner'
  );

  /*{ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! }*/
  function getVersion() {
    return new soapval('return', 'xsd:string', '1.0.1.14');
  }

  function setImage($username, $password, $base64Binary) {
    $con = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD) or die("Failed to connect to MySQL: ".mysql_error());
    $db = mysql_select_db(DB_NAME, $con) or die("Failed to connect to MySQL: ".mysql_error());
    if (mysqli_connect_errno($con)) {
      echo "Failed to connect to MySQL: ".mysqli_connect_error();
    }
    else {
      $query = mysql_query("SELECT * FROM CameraWebservice WHERE userName = '".mysql_real_escape_string($username)."' AND passWord = '".mysql_real_escape_string($password)."'") or die(mysql_error());
      if(!$row = mysql_fetch_array($query)) {
        /* You need to register yourself at beginning */
        return new soapval('return', 'xsd:string', 'You need to register yourself at beginning');
	    }
	    else {
        $path = 'screenshots/'.$row['userName'].'/';

        if (!file_exists($path)) {
          mkdir($path, 0755, true);
        }
        $result = file_put_contents($path.'liveImage.jpg', base64_decode($base64Binary));
        return new soapval('return', 'xsd:string', 'your session code is: '.$result);
	    }
    }
  }

  function getImage($username, $password) {
    $con = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD) or die("Failed to connect to MySQL: ".mysql_error());
    $db = mysql_select_db(DB_NAME, $con) or die("Failed to connect to MySQL: ".mysql_error());
    if (mysqli_connect_errno($con)) {
      echo "Failed to connect to MySQL: ".mysqli_connect_error();
    }
    else {
      $query = mysql_query("SELECT * FROM CameraWebservice WHERE userName = '".mysql_real_escape_string($username)."' AND passWord = '".mysql_real_escape_string($password)."'") or die(mysql_error());
      if(!$row = mysql_fetch_array($query)) {
        /* You need to register yourself at beginning */
        return new soapval('return', 'xsd:string', 'You need to register yourself at beginning');
	    }
	    else {
        $path = 'screenshots/'.$row['userName'].'/liveImage.jpg';
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        return new soapval('return', 'xsd:base64Binary', 'data:image/'.$type.';base64,'.base64_encode($data));
	    }
    }
  }

  function regUser($fullname, $username, $emailaddress, $password) {
    $con = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD) or die("Failed to connect to MySQL: ".mysql_error());
    $db = mysql_select_db(DB_NAME, $con) or die("Failed to connect to MySQL: ".mysql_error());
    if (mysqli_connect_errno($con)) {
      echo "Failed to connect to MySQL: ".mysqli_connect_error();
    }
    else {
      $query = mysql_query("SELECT * FROM CameraWebservice WHERE userName = '".mysql_real_escape_string($username)."'") or die(mysql_error());
      if (!$row = mysql_fetch_array($query)) {
        /* You need to register yourself at beginning */
        $query = "INSERT INTO CameraWebservice (fullName, userName, emailAddress, passWord) VALUES ('".mysql_real_escape_string($fullname)."', '".mysql_real_escape_string($username)."', '".mysql_real_escape_string($emailaddress)."', '".mysql_real_escape_string($password)."')";
        $data = mysql_query($query) or die(mysql_error());
	      if ($data) {
          return new soapval('return', 'xsd:string', 'Your registration is completed...');
	      }
	    }
	    else {
        return new soapval('return', 'xsd:string', 'Sorry, you are already registered user, choose another information...');
	    }
    }
  }

  function sendRequest($username, $password, $message) {
    $con = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD) or die("Failed to connect to MySQL: ".mysql_error());
    $db = mysql_select_db(DB_NAME, $con) or die("Failed to connect to MySQL: ".mysql_error());
    if (mysqli_connect_errno($con)) {
      echo "Failed to connect to MySQL: ".mysqli_connect_error();
    }
    else {
      $query = mysql_query("SELECT * FROM CameraWebservice WHERE userName = '".mysql_real_escape_string($username)."' AND passWord = '".mysql_real_escape_string($password)."'") or die(mysql_error());
      if (!$row = mysql_fetch_array($query)) {
        /* return false username not found */
        return new soapval('return', 'xsd:boolean', false);
	    }
	    else {
        $query = "INSERT INTO PSRequest (reqMessage) VALUES ('".mysql_real_escape_string($message)."')";
        $data = mysql_query($query) or die(mysql_error());
	      if ($data) {
          return new soapval('return', 'xsd:boolean', true);
	      }
	    }
    }
  }

  /*{ !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! }*/
  if (!isset($HTTP_RAW_POST_DATA))
    $HTTP_RAW_POST_DATA = file_get_contents("php://input");
  $server -> service($HTTP_RAW_POST_DATA);

?>