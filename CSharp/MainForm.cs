﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AForge.Video.DirectShow;
using System.Drawing.Imaging;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;
using AForge.Video;
using System.Diagnostics;
using AForge.Vision.Motion;
using CSharp.Webservice;
using System.Xml.Linq;
using System.Xml;

namespace CSharp
{
  public partial class MainForm : Form
  {
    private FilterInfoCollection VideoCaptureDevices;
    private VideoCaptureDevice FinalVideo;
    private Object isLock = new Object();
    private String ImagePath = @"cImages/";
    private String DateFormat = @"yyyy_MM_dd_h\_mm_ss_tt";
    private String PathFormat = "{0}\\{1}.jpg";
    private String RightNow = String.Empty;
    private CatchThiefPortTypeClient CatchThief = new CatchThiefPortTypeClient();

    static string ImageToBase64(Image image, ImageFormat format) {
      using (MemoryStream ms = new MemoryStream()) {
        image.Save(ms, format);
        byte[] imageBytes = ms.ToArray();
        String base64String = Convert.ToBase64String(imageBytes);
        return base64String;
      }
    }

    /*
    private MotionDetector MDetector = new MotionDetector(
            new TwoFramesDifferenceDetector(),
            new MotionAreaHighlighting());
    */

    private MotionDetector MDetector;
    private float MotionLevel = 0F;
    private float MotionCount = 0F;

    private static MotionDetector GetDefaultMotionDetector() {
      IMotionDetector iMDetector = null;
      IMotionProcessing iMProcessing = null;
      MotionDetector MDetector = null;

      iMDetector = new SimpleBackgroundModelingDetector() {
        DifferenceThreshold = 10,
        FramesPerBackgroundUpdate = 10,
        KeepObjectsEdges = true,
        MillisecondsPerBackgroundUpdate = 0,
        SuppressNoise = true
      };

      iMProcessing = new BlobCountingObjectsProcessing() {
        /* HighlightColor = System.Drawing.Color.Red, */
        HighlightColor = System.Drawing.Color.Transparent,
        HighlightMotionRegions = true,
        MinObjectsHeight = 10,
        MinObjectsWidth = 10
      };
      MDetector = new MotionDetector(iMDetector, iMProcessing);
      return (MDetector);
    }

    private bool IsDark(Bitmap CrudeBitmap) {
      if (CrudeBitmap == null)
        return true;
      var TendsToWhite = 0;
      var TendsToBlack = 0;
      var BlackAndWhiteBitmap = CrudeBitmap.Clone(new Rectangle(0, 0, CrudeBitmap.Width, CrudeBitmap.Height), PixelFormat.Format24bppRgb);
      for (int x = 0; x < BlackAndWhiteBitmap.Width - 1; x++) {
        for (int y = 0; y < BlackAndWhiteBitmap.Height - 1; y++) {
          var CurrentPixel = BlackAndWhiteBitmap.GetPixel(x, y);
          if (CurrentPixel.R < 150 && CurrentPixel.G < 150 && CurrentPixel.B < 150) {
            TendsToBlack++;
          }
          else if (CurrentPixel.R == 255 && CurrentPixel.G == 255 && CurrentPixel.B == 255) {
            TendsToWhite++;
          }
          else {
            return false;
          }
        }
      }
      return TendsToBlack > TendsToWhite;
    }

    private void GetMotionLevel(out float Level) {
      Level = Convert.ToSingle(MotionBar.Value) / 10;
    }

    private void GetImage(out Bitmap Image) {
      Image = new Bitmap(Preview.Image);
    }

    private void FinalVideo_NewFrame(object sender, NewFrameEventArgs eventArgs) {
      Bitmap video = (Bitmap)eventArgs.Frame.Clone();
      Preview.Image = video;
    }

    public MainForm() {
      InitializeComponent();
      Worker.DoWork += new DoWorkEventHandler(Worker_DoWork);
    }

    private void MainForm_Load(object sender, EventArgs e) {
      VideoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
      foreach (FilterInfo VideoCaptureDevice in VideoCaptureDevices) {
        DeviceList.Items.Add(VideoCaptureDevice.Name);
        DeviceList.SelectedIndex = 0;
      }
      FinalVideo = new VideoCaptureDevice(VideoCaptureDevices[DeviceList.SelectedIndex].MonikerString);
      FinalVideo.NewFrame += new NewFrameEventHandler(FinalVideo_NewFrame);
      FinalVideo.Start();
    }

    private void SaveAsImage_Click(object sender, EventArgs e) {
      Worker.RunWorkerAsync();
    }

    private void Worker_DoWork(object sender, DoWorkEventArgs e) {
      Bitmap CurrentImage = null;
      if (!Directory.Exists(ImagePath))
        Directory.CreateDirectory(ImagePath);

      MDetector = GetDefaultMotionDetector();
      while (!Worker.CancellationPending)
      {
        Invoke(new Action(() => GetImage(out CurrentImage)));
        Invoke(new Action(() => GetMotionLevel(out MotionCount)));
        lock (isLock)
        {
          if (!IsDark(CurrentImage)) {
            RightNow = DateTime.Now.ToString(DateFormat);
            MotionLevel = MDetector.ProcessFrame(CurrentImage);
            if(MotionLevel > MotionCount) {
              Graphics Painter = Graphics.FromImage(CurrentImage);
              Painter.DrawString(String.Format("Time: {0}", RightNow), new Font("Tahoma", 12f), Brushes.White, 2, 2);
              MotionPreview.Image = (Image) CurrentImage.Clone();
              CurrentImage.Save(String.Format(PathFormat, ImagePath, RightNow), ImageFormat.Jpeg);
              try {
                CatchThief.setImage(WSUsername.Text,
                                    WSPassword.Text,
                                    ImageToBase64(CurrentImage, ImageFormat.Jpeg));
              }
              catch {
                /* Console.WriteLine(RuntimeError.Message); */
              }
            }
          }
        }
        Thread.Sleep((int) TimeBySecond.Value * 1000);
      }
    }

    private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      Worker.Dispose();
      if (FinalVideo != null && FinalVideo.IsRunning) {
        FinalVideo.SignalToStop();
        FinalVideo = null;
      }
      Environment.Exit(0);
    }

    private void OpenSource_Click(object sender, EventArgs e) {
      Process.Start("https://en.wikipedia.org/wiki/Open_source");
    }

    private void Bitbucket_Click(object sender, EventArgs e) {
      Process.Start("https://bitbucket.org/SunAdobe/catchthief/");
    }

    private void CSharp_Click(object sender, EventArgs e) {
      Process.Start("https://en.wikipedia.org/wiki/C_Sharp_%28programming_language%29");
    }

    private void SunAdobe_Click(object sender, EventArgs e) {
      Process.Start("http://teamspeak.ciaddon.com/");
    }

    private void DeviceList_SelectedIndexChanged(object sender, EventArgs e) {
      Worker.Dispose();
      if(FinalVideo != null && FinalVideo.IsRunning) {
        FinalVideo.SignalToStop();
        FinalVideo = null;
      }
      FinalVideo = new VideoCaptureDevice(VideoCaptureDevices[DeviceList.SelectedIndex].MonikerString);
      FinalVideo.NewFrame += new NewFrameEventHandler(FinalVideo_NewFrame);
      FinalVideo.Start();
    }

    private void MotionBar_Scroll(object sender, EventArgs e) {
      ToolTip.SetToolTip(MotionBar, MotionBar.Value.ToString());
    }

    private void MoreInfo_Click(object sender, EventArgs e) {
      if (Size.Height == 345) {
        MoreInfo.Image = global::CSharp.Properties.Resources.Up;
        Size = new Size(Size.Width, 597);
        Location = new Point(Location.X, Location.Y - 126);
      }
      else {
        MoreInfo.Image = global::CSharp.Properties.Resources.Down;
        Size = new Size(Size.Width, 345);
        Location = new Point(Location.X, Location.Y + 126);
      }
    }

    private void MoreInfo_MouseHover(object sender, EventArgs e) {
      if (Size.Height == 345) {
        ToolTip.SetToolTip(MoreInfo, "نمایش امکانات بیشتر");
      }
      else {
        ToolTip.SetToolTip(MoreInfo, "حذف نمایش");
      }
    }

    private void DoOCR_Click(object sender, EventArgs e) {
      if (LanguageBox.SelectedIndex != -1) {
        Bitmap CurrentImage = null;
        Invoke(new Action(() => GetImage(out CurrentImage)));
        OCRPicture.Image = (Image) CurrentImage.Clone();
        OCRText.Lines = new String[] { CatchThief.ocrRequest(WSUsername.Text,
                                                             WSPassword.Text,
                                                             LanguageBox.SelectedItem.ToString(),
                                                             ImageToBase64(OCRPicture.Image, ImageFormat.Jpeg)) };
      }
      else {
        MessageBox.Show("انـتـخـاب زبــان خروجـی الـزامی اسـت", "تـوجـه", MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
    }

    private void OCRPicture_MouseClick(object sender, MouseEventArgs e) {
      OpenFileDialog OpenFile = new OpenFileDialog();
      OpenFile.InitialDirectory = @"C:\";
      OpenFile.Title = "Browse scanned document";
      OpenFile.CheckFileExists = true;
      OpenFile.CheckPathExists = true;
      OpenFile.DefaultExt = "jpg";
      OpenFile.Filter = "Joint Photographic Experts Group (*.jpg)|*.jpg";
      OpenFile.FilterIndex = 1;
      OpenFile.RestoreDirectory = true;
      OpenFile.ReadOnlyChecked = true;
      OpenFile.ShowReadOnly = true;
      if (OpenFile.ShowDialog() == DialogResult.OK) {
        OCRPicture.Image = new Bitmap(OpenFile.FileName);
      }
    }

  }
}
