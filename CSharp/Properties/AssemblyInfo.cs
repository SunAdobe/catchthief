﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Catch Thief")]
[assembly: AssemblyDescription("Free an application for catch ugly thief")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SunAdobe™")]
[assembly: AssemblyProduct("CatchThief")]
[assembly: AssemblyCopyright("Copyright © SunAdobe™ 2015 - 2023")]
[assembly: AssemblyTrademark("Trademark ® SunAdobe™ 2015 - 2023")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("371a35c5-5e63-463a-9ca7-08440de1c8d5")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.*")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]
