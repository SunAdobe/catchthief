﻿namespace CSharp {
  partial class MainForm {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if(disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
      this.Preview = new System.Windows.Forms.PictureBox();
      this.SaveAsImage = new System.Windows.Forms.Button();
      this.DeviceList = new System.Windows.Forms.ComboBox();
      this.Worker = new System.ComponentModel.BackgroundWorker();
      this.TimeBySecond = new System.Windows.Forms.NumericUpDown();
      this.TimeLabel = new System.Windows.Forms.Label();
      this.MotionPreview = new System.Windows.Forms.PictureBox();
      this.SunAdobe = new System.Windows.Forms.PictureBox();
      this.Bitbucket = new System.Windows.Forms.PictureBox();
      this.CSharp = new System.Windows.Forms.PictureBox();
      this.OpenSource = new System.Windows.Forms.PictureBox();
      this.MotionBar = new System.Windows.Forms.TrackBar();
      this.WSUsername = new System.Windows.Forms.TextBox();
      this.WSPassword = new System.Windows.Forms.TextBox();
      this.MotionLabel = new System.Windows.Forms.Label();
      this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this.MoreInfo = new System.Windows.Forms.Button();
      this.ShapeContainer = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
      this.Separator = new Microsoft.VisualBasic.PowerPacks.LineShape();
      this.OCRPicture = new System.Windows.Forms.PictureBox();
      this.LanguageBox = new System.Windows.Forms.ComboBox();
      this.LanguageLabel = new System.Windows.Forms.Label();
      this.DoOCR = new System.Windows.Forms.Button();
      this.OCRText = new System.Windows.Forms.RichTextBox();
      ((System.ComponentModel.ISupportInitialize)(this.Preview)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.TimeBySecond)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.MotionPreview)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.SunAdobe)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.Bitbucket)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.CSharp)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.OpenSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.MotionBar)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.OCRPicture)).BeginInit();
      this.SuspendLayout();
      // 
      // Preview
      // 
      this.Preview.BackColor = System.Drawing.Color.Silver;
      resources.ApplyResources(this.Preview, "Preview");
      this.Preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.Preview.Name = "Preview";
      this.Preview.TabStop = false;
      // 
      // SaveAsImage
      // 
      resources.ApplyResources(this.SaveAsImage, "SaveAsImage");
      this.SaveAsImage.Name = "SaveAsImage";
      this.SaveAsImage.UseVisualStyleBackColor = true;
      this.SaveAsImage.Click += new System.EventHandler(this.SaveAsImage_Click);
      // 
      // DeviceList
      // 
      this.DeviceList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.DeviceList.FormattingEnabled = true;
      resources.ApplyResources(this.DeviceList, "DeviceList");
      this.DeviceList.Name = "DeviceList";
      this.DeviceList.SelectedIndexChanged += new System.EventHandler(this.DeviceList_SelectedIndexChanged);
      // 
      // TimeBySecond
      // 
      resources.ApplyResources(this.TimeBySecond, "TimeBySecond");
      this.TimeBySecond.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.TimeBySecond.Name = "TimeBySecond";
      this.TimeBySecond.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
      // 
      // TimeLabel
      // 
      resources.ApplyResources(this.TimeLabel, "TimeLabel");
      this.TimeLabel.Name = "TimeLabel";
      // 
      // MotionPreview
      // 
      this.MotionPreview.BackColor = System.Drawing.Color.Silver;
      resources.ApplyResources(this.MotionPreview, "MotionPreview");
      this.MotionPreview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.MotionPreview.Name = "MotionPreview";
      this.MotionPreview.TabStop = false;
      // 
      // SunAdobe
      // 
      resources.ApplyResources(this.SunAdobe, "SunAdobe");
      this.SunAdobe.Cursor = System.Windows.Forms.Cursors.Hand;
      this.SunAdobe.Name = "SunAdobe";
      this.SunAdobe.TabStop = false;
      this.SunAdobe.Click += new System.EventHandler(this.SunAdobe_Click);
      // 
      // Bitbucket
      // 
      resources.ApplyResources(this.Bitbucket, "Bitbucket");
      this.Bitbucket.Cursor = System.Windows.Forms.Cursors.Hand;
      this.Bitbucket.Name = "Bitbucket";
      this.Bitbucket.TabStop = false;
      this.Bitbucket.Click += new System.EventHandler(this.Bitbucket_Click);
      // 
      // CSharp
      // 
      resources.ApplyResources(this.CSharp, "CSharp");
      this.CSharp.Cursor = System.Windows.Forms.Cursors.Hand;
      this.CSharp.Name = "CSharp";
      this.CSharp.TabStop = false;
      this.CSharp.Click += new System.EventHandler(this.CSharp_Click);
      // 
      // OpenSource
      // 
      resources.ApplyResources(this.OpenSource, "OpenSource");
      this.OpenSource.Cursor = System.Windows.Forms.Cursors.Hand;
      this.OpenSource.Name = "OpenSource";
      this.OpenSource.TabStop = false;
      this.OpenSource.Click += new System.EventHandler(this.OpenSource_Click);
      // 
      // MotionBar
      // 
      resources.ApplyResources(this.MotionBar, "MotionBar");
      this.MotionBar.LargeChange = 1;
      this.MotionBar.Name = "MotionBar";
      this.MotionBar.Value = 7;
      this.MotionBar.Scroll += new System.EventHandler(this.MotionBar_Scroll);
      // 
      // WSUsername
      // 
      resources.ApplyResources(this.WSUsername, "WSUsername");
      this.WSUsername.Name = "WSUsername";
      // 
      // WSPassword
      // 
      resources.ApplyResources(this.WSPassword, "WSPassword");
      this.WSPassword.Name = "WSPassword";
      // 
      // MotionLabel
      // 
      resources.ApplyResources(this.MotionLabel, "MotionLabel");
      this.MotionLabel.Name = "MotionLabel";
      // 
      // MoreInfo
      // 
      this.MoreInfo.Cursor = System.Windows.Forms.Cursors.Hand;
      this.MoreInfo.Image = global::CSharp.Properties.Resources.Down;
      resources.ApplyResources(this.MoreInfo, "MoreInfo");
      this.MoreInfo.Name = "MoreInfo";
      this.MoreInfo.UseVisualStyleBackColor = true;
      this.MoreInfo.Click += new System.EventHandler(this.MoreInfo_Click);
      this.MoreInfo.MouseHover += new System.EventHandler(this.MoreInfo_MouseHover);
      // 
      // ShapeContainer
      // 
      resources.ApplyResources(this.ShapeContainer, "ShapeContainer");
      this.ShapeContainer.Name = "ShapeContainer";
      this.ShapeContainer.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.Separator});
      this.ShapeContainer.TabStop = false;
      // 
      // Separator
      // 
      resources.ApplyResources(this.Separator, "Separator");
      this.Separator.Name = "Separator";
      // 
      // OCRPicture
      // 
      this.OCRPicture.BackColor = System.Drawing.Color.Silver;
      resources.ApplyResources(this.OCRPicture, "OCRPicture");
      this.OCRPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.OCRPicture.Name = "OCRPicture";
      this.OCRPicture.TabStop = false;
      this.OCRPicture.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OCRPicture_MouseClick);
      // 
      // LanguageBox
      // 
      this.LanguageBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.LanguageBox.DropDownWidth = 120;
      this.LanguageBox.FormattingEnabled = true;
      this.LanguageBox.Items.AddRange(new object[] {
            resources.GetString("LanguageBox.Items"),
            resources.GetString("LanguageBox.Items1")});
      resources.ApplyResources(this.LanguageBox, "LanguageBox");
      this.LanguageBox.Name = "LanguageBox";
      // 
      // LanguageLabel
      // 
      resources.ApplyResources(this.LanguageLabel, "LanguageLabel");
      this.LanguageLabel.Name = "LanguageLabel";
      // 
      // DoOCR
      // 
      resources.ApplyResources(this.DoOCR, "DoOCR");
      this.DoOCR.Name = "DoOCR";
      this.DoOCR.UseVisualStyleBackColor = true;
      this.DoOCR.Click += new System.EventHandler(this.DoOCR_Click);
      // 
      // OCRText
      // 
      this.OCRText.BackColor = System.Drawing.SystemColors.MenuBar;
      resources.ApplyResources(this.OCRText, "OCRText");
      this.OCRText.Name = "OCRText";
      this.OCRText.ShortcutsEnabled = false;
      // 
      // MainForm
      // 
      resources.ApplyResources(this, "$this");
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.OCRText);
      this.Controls.Add(this.DoOCR);
      this.Controls.Add(this.LanguageBox);
      this.Controls.Add(this.OCRPicture);
      this.Controls.Add(this.MoreInfo);
      this.Controls.Add(this.MotionLabel);
      this.Controls.Add(this.WSPassword);
      this.Controls.Add(this.WSUsername);
      this.Controls.Add(this.MotionBar);
      this.Controls.Add(this.OpenSource);
      this.Controls.Add(this.CSharp);
      this.Controls.Add(this.Bitbucket);
      this.Controls.Add(this.SunAdobe);
      this.Controls.Add(this.MotionPreview);
      this.Controls.Add(this.TimeBySecond);
      this.Controls.Add(this.DeviceList);
      this.Controls.Add(this.SaveAsImage);
      this.Controls.Add(this.Preview);
      this.Controls.Add(this.TimeLabel);
      this.Controls.Add(this.LanguageLabel);
      this.Controls.Add(this.ShapeContainer);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "MainForm";
      this.Opacity = 0.9D;
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
      this.Load += new System.EventHandler(this.MainForm_Load);
      ((System.ComponentModel.ISupportInitialize)(this.Preview)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.TimeBySecond)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.MotionPreview)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.SunAdobe)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.Bitbucket)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.CSharp)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.OpenSource)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.MotionBar)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.OCRPicture)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox Preview;
    private System.Windows.Forms.Button SaveAsImage;
    private System.Windows.Forms.ComboBox DeviceList;
    private System.ComponentModel.BackgroundWorker Worker;
    private System.Windows.Forms.NumericUpDown TimeBySecond;
    private System.Windows.Forms.Label TimeLabel;
    private System.Windows.Forms.PictureBox MotionPreview;
    private System.Windows.Forms.PictureBox SunAdobe;
    private System.Windows.Forms.PictureBox Bitbucket;
    private System.Windows.Forms.PictureBox CSharp;
    private System.Windows.Forms.PictureBox OpenSource;
    private System.Windows.Forms.TrackBar MotionBar;
    private System.Windows.Forms.TextBox WSUsername;
    private System.Windows.Forms.TextBox WSPassword;
    private System.Windows.Forms.Label MotionLabel;
    private System.Windows.Forms.ToolTip ToolTip;
    private System.Windows.Forms.Button MoreInfo;
    private Microsoft.VisualBasic.PowerPacks.ShapeContainer ShapeContainer;
    private Microsoft.VisualBasic.PowerPacks.LineShape Separator;
    private System.Windows.Forms.PictureBox OCRPicture;
    private System.Windows.Forms.ComboBox LanguageBox;
    private System.Windows.Forms.Label LanguageLabel;
    private System.Windows.Forms.Button DoOCR;
    private System.Windows.Forms.RichTextBox OCRText;
  }
}

